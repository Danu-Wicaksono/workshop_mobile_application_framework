void main(List<String> args) {
  // if else
  var isThisDanu = true;

  if (isThisDanu) {
    print("benar");
  } else {
    print("salah");
  }
  print("==========================");

  // tenary operator
  var isThisDanu2 = false;
  isThisDanu2 ? print("benar") : print("salah");
  print("==========================");

  if (true) {
    print("jalankan code");
  }

  if (false) {
    print("Program tidak jalan code");
  }
  print("==========================");

  var mood = "sedih";
  if (mood == "sedih") {
    print("hari ini aku sedih:)");
  }
  print("==========================");

  // branching
  var minimarketStatus = "open";
  if (minimarketStatus == "open") {
    print("saya akan membeli Laptop asus ROG");
  } else {
    print("minimarketnya tutup");
  }
  print("==========================");

  // branching dengan kondisi
  var minimarketStatus2 = "close";
  var minuteRemainingToOpen = 5;
  if (minimarketStatus2 == "open") {
    print("saya akan membeli laptop asus ROG");
  } else if (minuteRemainingToOpen <= 5) {
    print("Toko buka sebentar lagi, saya tungguin");
  } else {
    print("Toko tutup, saya pulang lagi");
  }
  print("==========================");

  // conditional bersarang
  var minimarketStatus3 = "open";
  var telur = "soldout";
  var buah = "soldout";
  if (minimarketStatus3 == "open") {
    print("Danu pergi kepasar beli telur dan buah");
    if (telur == "soldout" || buah == "soldout") {
      print("belanjaan saya tidak lengkap");
    } else if (telur == "soldout") {
      print("telur habis");
    } else if (buah == "soldout") {
      print("buah habis");
    }
  } else {
    print("pasar tutup, saya pulang lagi");
  }
  print("==========================");

  // conditional dengan switch case
  var buttonPushed = 1;
  switch (buttonPushed) {
    case 1:
      {
        print('matikan TV!');
        break;
      }
    case 2:
      {
        print('turunkan volume TV!');
        break;
      }
    case 3:
      {
        print('tingkatkan volume TV!');
        break;
      }
    case 4:
      {
        print('matikan suara TV!');
        break;
      }
    default:
      {
        print('Tidak terjadi apa-apa');
      }
  }
}
