void main(List<String> args) async {
  print("persiapan");
  print(await tes1());
  print(await line1());
  print(await line2());
  print(await line3());
  print(await line4());
  print(await line5());
  print(await line6());
  print(await line7());
  print(await line8());
}

Future<String> tes1() async {
  String greeting = "1,2,3 Mulai";
  return await Future.delayed(Duration(seconds: 2), () => (greeting));
}

Future<String> line1() async {
  String greeting = "Lir ilir lir ilir tandure wong sumilir tak ijo royo royo";
  return await Future.delayed(Duration(seconds: 3), () => (greeting));
}

Future<String> line2() async {
  String greeting = "Tak sengguh panganten anyar";
  return await Future.delayed(Duration(seconds: 4), () => (greeting));
}

Future<String> line3() async {
  String greeting = "Cah angon cah angon penekna blimbing kuwi";
  return await Future.delayed(Duration(seconds: 3), () => (greeting));
}

Future<String> line4() async {
  String greeting = "Lunyu lunyu penekna kanggo mbasuh dodotira";
  return await Future.delayed(Duration(seconds: 3), () => (greeting));
}

Future<String> line5() async {
  String greeting = "Dodotira dodotira kumintir bedah ing pinggir";
  return await Future.delayed(Duration(seconds: 2), () => (greeting));
}

Future<String> line6() async {
  String greeting = "Dondomana jrumatana kanggo seba mengko sore";
  return await Future.delayed(Duration(seconds: 2), () => (greeting));
}

Future<String> line7() async {
  String greeting = "Mumpung padang rembulane mumpung jembar kalangane";
  return await Future.delayed(Duration(seconds: 2), () => (greeting));
}

Future<String> line8() async {
  String greeting = "Sun suraka surak hiyo";
  return await Future.delayed(Duration(seconds: 2), () => (greeting));
}
